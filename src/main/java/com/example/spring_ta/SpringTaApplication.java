package com.example.spring_ta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringTaApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringTaApplication.class, args);
    }

}
